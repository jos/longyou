This is a CCRMA GitLab repo containing openly shared resources for the CCRMA Longyou Grottoes Recording Project.
It lives at [``https://cm-gitlab.stanford.edu/jos/longyou/``](https://cm-gitlab.stanford.edu/jos/longyou/)

To make a copy, type, in a Terminal (shell),

``git clone git@cm-gitlab.stanford.edu:jos/longyou.git``

Enjoy!

## Preliminary Test Recordings March 29, 2017

This preliminary test consists of stereo recordings for determining
general characteristics of the various spaces (see Recording Notes
below).  A more detailed recording effort is planned using tetramics
for 3D capture.

### Contents

* Balloon_and_Microphone_Position.JPG - sketches showing microphone and balloon placement
* Grotto*.JPG - photos of the grottoes
* LongYou_Balloon_Pop_Grotto*.wav - stereo balloon-pop recordings

## Recording Notes

The microphone and balloon-pop locations were chosen to maximize
smoothness of response based on walking around and clapping and
listening.  In other words, we were "mining impulse responses" for
musical reverberation purposes in these spaces.  The main source of
non-smoothness was large flat surfaces causing a discrete "slapback"
effect.  If there is only one such surface, it is effective to move
close to it, since a sufficiently quick echo fuses perceptually as an
early reflection, perceived as coloration as opposed to echo.  Another
technique is to use the pillars to shadow the large flat surfaces.
The pillars are rounded-wedge shaped making them good as diffuse
deflectors.

In principle, corners are ideal because one is simultaneously close to
all three main slapback surfaces.  Another theoretical consideration
is that only in a corner, being an anti-node for all resonant modes,
can one excite and record all modes of the space simultaneously.
However, we found the corners to lack spatial impression, and so we
chose instead to find good places to excite and record in the
interiors of the spaces, typically near one wall, pillar(s), or other
obstruction(s).
