// hp.dsp = highpass filter

fi = library("filters.lib");

// fc = hslider("[1] fc [unit:Hz]",100,20,10000,1);

fc = 1000;   // Hz

//t: process = 1-1' : fi.highpass(5,fc);
process = fi.highpass(5,fc);
